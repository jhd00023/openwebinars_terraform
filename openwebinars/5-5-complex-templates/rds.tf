resource "aws_db_subnet_group" "subn-groups" {
#subnets donde queremos que se levante el RDS
  subnet_ids = ["${aws_subnet.pri1.id}","${aws_subnet.pri2.id}"]
}
resource "aws_dbs_instance" "mydb" {
  instance_class = "db.t2.micro"
  identifier = "mydb"
  username = "${var.rds_username}"
  password = "${var.rds_passwd}"
  engine  = "mysql"
  allocated_storage = 10
  storage_type = "gp2"

  multi_az = false
  db_subnet_group_name="${aws_db_subnet_group.subn-groups.name}"
  vpc_security_groups_ids = ["${aws_security_group.rds-sg.id}"]
  #accesible publicamente on IP publica
  publicly_accesible = true
  #veremos mas adelante
  skip_final_snapshot = true

}