resource "aws_elb" "elb-web" {
  name = "${var.environment}-${var.project}"
  # hace el balanceo entre las 3 zonas
  cross_zone_load_balancing = true
  subnets = ["${aws_subnet.pub1.id}", "${aws_subnet.pub2.id}"]

  "listener" {
    instance_port = 50
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"

  }
  #le asociamos al security group al que queremos que pertenezca
  security_groups = ["${aws_security_group.elb-sg.id}"]
  #referenciamos las instancias creadas con un count con el asterisco (todos los id de la lista)
  instances = ["${aws_instance.webservers.*.id}"]
}
