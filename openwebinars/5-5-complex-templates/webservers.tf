resource "aws_instance" "webservers" {
  #le decimos la AMI y la región donde se alojará:
  ami = "${lookup(var.aws_amis, var.aws_region )}"
  #tipo de instancia que está definida en las variables como t2.micro
  instance_type = "${var.instance_type}"
  #grupo de seguridad al que pertenede dnetro del vpc
  vpc_security_group_ids = ["${aws_security_group.web-sg.id}"]
  #le marcamos una de la subnet privadas
  subnet_id = "${aws_subnet.pri1.id}"
  #le decimos el numero de webservers a lanzar
  count = 2
  #Tageamos las instancias.
  tags {
    Name = "${var.environment}-webservers"
  }
}