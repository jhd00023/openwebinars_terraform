terraform{
  required_version = ">= 0.11.3"
}
provider "aws" {
  region = "eu-west-1"
  allowed_account_ids = ["417972487289"]
  profile = "openwebinars"

}
