resource "aws_security_group" "elb-sg" {
  name = "elb-sg"
  vpc_id = "${aws_vpc.vpc.id}"
  ingress {#esto es para le balzanceador de cargas por el pto 80
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]

  }
  egress {
    #cualquier puerto
    from_port = 0
    #cualquier protocolo
    protocol = "-1"
    to_port = 0
    #todas las direcciones.
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_security_group" "web-sg" {
  name = "web-sg"
  vpc_id = "${aws_vpc.vpc.id}"
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    #Esta regla permite conectar solo a los security groups que se mencionen como regla de entrada
    #el trafico está cerrado al resto de maquinas.
    security_groups = ["${aws_security_group.elb-sg.id}"]
  }
  egress {
    #cualquier puerto
    from_port = 0
    #cualquier protocolo
    protocol = "-1"
    to_port = 0
    #todas las direcciones.
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "rds-sg" {
  name = "rds-sg"
  vpc_id = "${aws_vpc.vpc.id}"
  ingress {
    #puerto mysql tcp
    from_port = 3306
    protocol = "tcp"
    to_port = 3306
    #definimos solo coneciones desde los security
    security_groups = ["${aws_security_group.web-sg.id}"]
  }
  #el siguiente ingress representa la IP de la oficina desde donde conectamos
  ingress {
    #puerto mysql tcp
    from_port = 3306
    protocol = "tcp"
    to_port = 3306
    #definimos solo coneciones desde los security
    cidr_blocks = ["37.223.233.174/32"]
  }
    egress {
    #cualquier puerto
    from_port = 0
    #cualquier protocolo
    protocol = "-1"
    to_port = 0
    #todas las direcciones.
    cidr_blocks = ["0.0.0.0/0"]
  }


}