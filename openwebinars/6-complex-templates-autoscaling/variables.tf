## tipo de instancia
variable "instance_type" {
  type = "string"
  default = "t2.micro"
}
#region de irlanda
variable "region" {
  type = "string"
  default = "eu-west-1"
}
#asociamos la ami de linux amazon según la zona donde se cree
variable "aws_amis" {
  type = "map"
  default = {
    "eu-west-1" = "ami-d834aba1" #irlanda
    "us-east-1" = "ami-97785bed" #virginia EEUU
    "eu-central-1" = "ami-5652ce39" #francfurt
  }
}
#este es el ID de la cuenta de amazon
variable "aws_id"{
  type = "string"
  default = "417972487289"
}
#es un indice para crear automaticamente los nombres de las instancias
#prod[tipo_instancia]whatever prot=producción
variable "environment" {
  type = "string"
  default = "prod"
}
#para crear una cadena de caracteres al final del nombre de la instancia
variable "project" {
  type = "string"
  default = "web"
}

variable "aws_region" {
  type = "string"
  default = "eu-west-1"
}

variable "cidr" {
  type = "string"
  default = "10.0.0.0/16"
}
#a continuación las variables de los CIDR de las subnets
variable "pub1_cidr" {
  type = "string"
  default = "10.0.0.0/24"
}
variable "pub2_cidr" {
  type = "string"
  default = "10.0.1.0/24"
}
variable "pri1_cidr" {
  type = "string"
  default = "10.0.10.0/24"
}
variable "pri2_cidr" {
  type = "string"
  default = "10.0.11.0/24"
}

#variables para el login de la base de datos, usuario y contraseña
variable "rds_username" {
  type = "string"
  default = "root"
}
variable "rds_passwd" {
  type = "string"
  default = "0penW3b1n4r$"
}

