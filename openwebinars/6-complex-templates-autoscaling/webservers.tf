# vamos a definir los servidores web en base a un autoscalling
resource "aws_launch_configuration" "web-server" {
  name_prefix = "web-server"
  image_id = "${lookup(var.aws_amis, var.aws_region)}"
  instance_type = "${var.instance_type}"
  key_name = "openwebinars"
  security_groups = ["${aws_security_group.web-sg.id}"]
  user_data = "${file("templates/userdata.tpl")}"
  #manejar ciclos de vida
  lifecycle {
    #si hay un cambio en el que hay que eliminar, antes de eliminar se crea uno n uevo
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "as-web" {
  name = "asg-web"

  launch_configuration = "${aws_launch_configuration.web-server.name}"
  #crea una sola instancia del balanceador
  max_size = 1
  min_size = 1
  #añade el balanceador de cargas
  load_balancers = ["${aws_elb.elb-web.id}"]
  vpc_zone_identifier = ["${aws_subnet.pub1.id}","${aws_subnet.pub2.id}"]
  #hasta que no da señales de vida no da por buena la creación de la instancia.
  wait_for_elb_capacity = 1
  tag {
    key ="Name"
    #se propagan al arranque
    propagate_at_launch = true
    value = "web-server"
  }
  lifecycle {
    #si hay un cambio en el que hay que eliminar, antes de eliminar se crea uno n uevo
    create_before_destroy = true
  }

}