terraform{
  required_version = ">= 0.11.3"
}
provider "aws" {
  region = "eu-west-1"//irlanda
  allowed_account_ids = ["417972487289 "]//id de la cuenta
  profile = "openwebinars"

}
resource "aws_vpc" "vpc_terraform" {
    cidr_block = "10.0.0.0/16"
    enable_dns_hostnames = true
    enable_dns_hostnames = true
    tag {
      Name ="OpenwebinarsTerraform"
    }
}