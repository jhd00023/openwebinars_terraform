variable "instance_type" {
  type = "string"
  default = "t2.micro"
}
variable "region" {
  type = "string"
  default = "eu-west-1"
}
variable "aws_amis" {
  type = "map"
  default = {
    "eu-west-1" = "ami-d834aba1" #irlanda
    "us-east-1" = "ami-97785bed" #virginia EEUU
    "eu-central-1" = "ami-5652ce39" #francfurt
  }
}
variable "aws_id"{
  type = "string"
  default = "417972487289"
}

variable "project" {
  type = "string"
  default = "ow-web"
}
variable "environment" {
  type = "string"
  default = "prod"
}